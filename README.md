# Projet WAN Packet Tracer
*Julie Bousseau et Alexandre Doyen*

## 1) Contexte
Le projet consiste à proposer une solution technique sur le logiciel Cisco Packet Tracer pour interconnecter les différents bâtiments d'une chaîne de 3 hôtels.

Plus précisément, la chaîne est implantée dans trois villes (Rennes, Lannion et Quimper), où respectivement, il y a :
* Pour Rennes :
  * Un immeuble de cinquante chambres
  * Le WiFI disponible dans tous le bâtiment pour les clients
  * Une connexion ADSL doublée permettant de redonder la connexion réseau en cas de problème
  * Un hébergement WEB permettant de contenir le site WEB de la chaîne
  * Une base de données locale stockant les informations sur les différentes chambres
* Pour Lannion :
  * Trois bâtiments distincts contenant chacun vingt chambres
  * Le WiFi dans chacun des bâtiments
  * Une base de données locale à l'instar du site de Rennes
  * Une connexion ADSL simple
* Pour Quimper :
  * Une seul bâtiment de vingt chambres
  * Une base de données locale à l'instar des deux sites précédents
  * Une connexion ADSL simple

## 2) Nos choix d'implémentation

### 2.1) Carthographie générale des sites

![Cartographie](Cartographie.png)

### 2.2) Architecture des réseaux locaux

Voici le plan d'adressage correspondant aux différents réseaux locaux relatifs aux trois sites :
|Site                 |CIDR du sous-réseau|Nom du sous-réseau                                 |
|---------------------|-------------------|---------------------------------------------------|
|Rennes - 10.0.0.0/16 |10.0.1.0/24        |Connexion opérateur                                |
|                     |10.0.2.0/24        |DMZ                                                |
|                     |10.0.3.0/24        |Réseau local                                       |
|                     |10.0.4.0/24        |Réseau WiFi des clients                            |
|                     |10.0.5.0/24        |Réseau d'interconnexion des bornes WiFi des clients|
|Lannion - 10.1.0.0/16|10.1.1.0/24        |Connexion opérateur                                |
|                     |10.1.2.0/24        |Réseau d'interconnexion des bornes WiFi des clients|
|                     |10.1.3.0/24        |Réseau local                                       |
|                     |10.1.4.0/24        |Réseaux WiFi des clients                           |
|Quimper - 10.2.0.0/16|10.2.1.0/24        |Connexion opérateur                                |
|                     |10.2.2.0/24        |Réseau d'interconnexion des bornes WiFi des clients|
|                     |10.2.3.0/24        |Réseau local                                       |
|                     |10.2.4.0/24        |Réseau WiFi des clients                            |

Et voici les schémas généraux de notre réseau :
![Schema du réseau de Rennes](SchemaReseau1.png)
![Schema du réseau de Lannion](SchemaReseau2.png)
![Schema du réseau de Quimper](SchemaReseau3.png)

### 2.3) Architecture du réseau Opérateur

Étant donné que l'on dispose de deux opérateurs différents, il a fallu mettre en place deux réseaux opérateurs. Ainsi, pour simuler un vériable réseau WAN, le premier opérateur distribue des adresses IPs à chaque connexion en utilisant des réseaux IPs de deux adresses, comme le montre cet extrait de configuration :
```
ip dhcp pool GLIANTIC_HOTELS_RENNES
 network 35.85.175.0 255.255.255.252
ip dhcp pool GLIANTIC_HOTELS_LANNION
 network 185.65.15.0 255.255.255.252
ip dhcp pool GLIANTIC_HOTELS_QUIMPER
 network 178.54.76.0 255.255.255.252
```

Ensuite, les opérateurs sont interconnectés entre-eux grâce à un switch, et sur le réseau d'interconnexion, chaque opérateur dispose de sa propre adresse IP incorporée dans la configuration des interfaces concernées :
```
interface FastEthernet0/0
 ip address 10.0.0.1 255.0.0.0
 duplex auto
 speed auto
```

#### 2.4) Mise en place du VPN site-à-site

L'idée est que chacun des sites puissent communiquer à l'aide d'un VPN site-à-site. Ainsi, par exemple, pour communiquer de l'hôtel de Lannion à celui de Quimper, les communications passeaient par le site de Rennes, connecté en VPN à celui de Lannion et celui de Quimper.

Cependant, sous Cisco Packet Tracer v7.3.1.0362, il est impossible de mettre en place un tel réseau dû à un manque de commandes, donc ce qui suit va être une implémentation de ce qui aurait dû être fait sur du vrai matériel afin de mettre en place une telle architecture entre Rennes et Lannion. Tout va se passer au niveau des routeurs de connexion opérateur.

Tout d'abord, il aurait fallu activer les modules de sécurité permettant d'utiliser la cryptographie pour sécuriser les échanges au travers du tunnel VPN :
```
Rennes>enable
Rennes#conf t
Rennes(config)#license boot module c2900 technology-package securityk9
Rennes(config)#exit
Rennes#copy run start
Rennes#reload
```

Ensuite, il aurait fallu configurer le chiffrement à l'aide de ISAKMP (Internet Security Association and Key Management Protocol) :
```
Rennes(config)#crypto isakmp policy 10
Rennes(config-isakmp)# encryption aes
Rennes(config-isakmp)# authentication pre-share
Rennes(config-isakmp)# group 5
Rennes(config-isakmp)#exit
```

Après, il aurait fallu configurer les différentes clés de chiffrement :
```
Rennes(config)#crypto isakmp key VPN address 83.0.0.1
Rennes(config)#crypto ipsec transform-set CRYPSET esp-aes esp-sha-hmac
```

À fortiori, il faut configurer les ACLs (Access List) permettant savoir quels flux sont à translater en NAT ou pas :
```
Rennes(config)#ip access-list standard 1
Rennes(config-std-nacl)# no permit ip 10.0.0.0 0.0.255.255
Rennes(config-std-nacl)# permit ip 10.0.0.0 0.255.255.255
Rennes(config-std-nacl)# exit
```

Finalement, il faut faire ceci pour mettre en place le VPN sur le routeur de Rennes :
```
Rennes(config)#crypto map VPN_MAP 10 ipsec-isakmp
Rennes(config-crypto-map)# set peer 185.65.15.2
Rennes(config-crypto-map)# set transform-set CRYPSET
Rennes(config-crypto-map)# match address standard 1
Rennes(config-crypto-map)#exit
Rennes(config)#int FastEthernet 8/0
Rennes(config-if)#crypto map VPN_MAP
```

Ensuite, sur le routeur de Lannion, il faudrait réaliser les mêmes opérations en changeant la paire pour celui de Rennes, et en adaptant la configuration des interfaces.

#### 2.5) Mise en place des règles de sécurité pour éviter que les clients aient accès au réseau local

Dans le cas de l'hôtel de Rennes, ces règles sont mises en place au niveau du switch de niveau 3 qui interconnecte tous les équipements du site. Ainsi, dans un premier temps, l'ACL (Access List) 1 a été configurée :
```
Rennes(config)#access-list 1 deny 10.0.3.0 0.0.0.255
Rennes(config)#access-list 1 deny 10.1.3.0 0.0.0.255
Rennes(config)#access-list 1 deny 10.2.3.0 0.0.0.255
Rennes(config)#access-list 1 permit any
```

Dans cette ACL, on peut remarquer que tous les flux à destination des réseaux locaux des trois sites sont bloqués, et tout le reste du traffic est autorisé.

Ensuite, il faut l'appliquer à l'interface du VLAN conerné (Ici, 40 comme c'est celu permettant de connecter la borne WiFi) :
```
Rennes(config)#int vlan40
Rennes(config-if)#ip access-group 1 out
```

## 3) Implémenation des réseaux des différents hôtels

### 3.1) Hôtel principal : Rennes

#### 3.1.1) Redondance de la connexion réseau

Dans le cahier des charges était stipulé la nécessité de disposer d'une connexion ADSL redondée, afin de garantir le fonctionnement de la chaîne d'hôtels en cas de panne de l'un des deux opérateurs.

Ainsi, en utilisant deux routes par défaut au niveau du routeur de connexion à l'opérateur de Rennes, celui-ci va choisir la première route si elle est disponible, ou sinon, va utiliser la deuxième. D'un point de vue de la configuration, cela se traduit ainsi :
```
Rennes(config)#ip route 0.0.0.0 0.0.0.0 FastEthernet8/0
Rennes(config)#ip route 0.0.0.0 0.0.0.0 FastEthernet9/0
```

#### 3.1.2) Implémentation de la DMZ

En théorie, une DMZ est une règle de routage sur un routeur faisant du NAT sur laquelle le trafic entrant sur un port spécifique (Par exemple, le port TCP 80/HTTP) est redirigé sur une machine spécifique du réseau, et sur un port spécifique de cette même machine.

### 3.2) Hôtels secondaires : Lannion et Quimper

Comme expliqué précédemment, dans l'idée initiale, les hôtels de Lannion et Quimper auraient dû être connectés en VPN à celui de Rennes. Cependant, à cause des limitations liées à Packet Tracer, nous n'avons pas pu le faire.

Nonobstant, nous avons pu quand même configurer les réseaux locaux associés !

Pour le site de Lannion, la spécificité est qu'il y a un bâtiment principal et deux bâtiments satellites. Ainsi, pour relier les deux derniers bâtiments au premier, il a fallu utiliser une fibre optique, car l'atténuation du signal sur un câble éthernet était telle que le signal ne passait plus au travers.

# 4) Propositions et pistes d'amélioration

Actuellement, les PCs d'un même bâtiment peuvent communiquer entre eux, ce qui n'est pas sécurisé. A l'avenir, il serait préférable d'empêcher les paquets d'atteindre les autres PCs, mais ce n'est pas implémenté dans notre solution, car les routeurs WiFi n'implémentaient pas cette fonctionnalité.
